# Juegos Android, por Victor Laguillo y Alba Callejas#

# Índice #
1. [__Estudio inicial__](https://gitlab.com/vlaguillo/Juegos_android/blob/master/Documentaci%C3%B3n.md#1-estudio-inicial)
2. [__Análisis del sistema__](https://gitlab.com/vlaguillo/Juegos_android/blob/master/Documentaci%C3%B3n.md#2-análisis-del-sistema)
3. [__Herramientas y conceptos__](https://gitlab.com/vlaguillo/Juegos_android/blob/master/Documentaci%C3%B3n.md#3-herramientas-y-conceptos)
4. [__Diseño del sistema__](https://gitlab.com/vlaguillo/Juegos_android/blob/master/Documentaci%C3%B3n.md#4-diseño-del-sistema)
5. [__Desarrollo__](https://gitlab.com/vlaguillo/Juegos_android/blob/master/Documentaci%C3%B3n.md#5-desarrollo)
    + [__Clicker__](https://gitlab.com/vlaguillo/Juegos_android/blob/master/Documentaci%C3%B3n.md#clicker)
    + [__Casino__](https://gitlab.com/vlaguillo/Juegos_android/blob/master/Documentaci%C3%B3n.md#casino)
    + [__Brick Breaker__](https://gitlab.com/vlaguillo/Juegos_android/blob/master/Documentaci%C3%B3n.md#brick-breaker)
6. [__Implantación__](https://gitlab.com/vlaguillo/Juegos_android/blob/master/Documentaci%C3%B3n.md#6-implantación)
7. [__Mantenimiento__](https://gitlab.com/vlaguillo/Juegos_android/blob/master/Documentaci%C3%B3n.md#7-mantenimiento)
8. [__Problemas__](https://gitlab.com/vlaguillo/Juegos_android/blob/master/Documentaci%C3%B3n.md#8-problemas)
9. [__Conclusiones__](https://gitlab.com/vlaguillo/Juegos_android/blob/master/Documentaci%C3%B3n.md#9-conclusiones)
10. [__Bibliografia__](https://gitlab.com/vlaguillo/Juegos_android/blob/master/Documentaci%C3%B3n.md#10-bibliografia)

## 1. Estudio inicial ##
Game Maker Studio 2 es un software para desarrollar videojuegos, su desarrollador es la compañia de Yoyo Games Ltd. Originalmente fue llamado Animo y fue lanzado el 15 de noviembre de 1999, es el 2 de octubre de 2004 cuando es lanzado Game Maker 6.0 y el 8 de marzo de 2017 lanzan la primera versión estable de Game Maker Studio 2.

## 2. Análisis del sistema ##
Los requisítos mínimos para hacer funcionar el programa de Game Maker Studio 2 son:
+ 64bits Intel, compatible con Dual Core CPU
+ 2GB de RAM
+ DX11 para tarjeta gráfica
+ SO Windows 7 64bits
+ Mínimo 3GB libre de disco duro.


## 3. Herramientas y conceptos
Game maker y conocimientos básicos de programación.

## 4. Diseño del sistema ##
Para diseñar nuestra aplicación hemos usado las herramientas que ofrecía el mismo Game Maker.

## 5. Desarrollo ##

### Palabras claves y definiciones ###

Antes de empezar con el desarrollo, explicaremos algunos términos que hay que tener en cuenta:
+ __Sprite__: Le llamamos sprite a la apariencia visual que tomará el objeto, no podemos implementar código.
+ __Objeto__: Le llamamos objeto al elemento físico que toma como apariencia al sprite, se le puede implementar código.
+ __Instancia__: Son copias del objeto.
+ __Room__: Es el espacio que ocupa la sala del juego.
+ __Eventos__: Acciones que se desarrollan dentro de un objeto.
    + __Crear__: Se ejecuta el código cuando se crea una instancia por primera vez.
    + __Paso__: Se ejecuta o comprueba el código durante todos los instantes en el que el juego está en marcha.
    + __Dibujar__: Permite pintar sprites y texto.
    + __Pulsación__: Se ejecuta el código cuando se pulsa un botón (el que tu establezcas).
    + __Entrada de ratón__: Se ejecuta el código cuando detecta entrada de ratón.
    + __Alarma__: Solo se ejecuta cuando se la llama previamente, ponemos una acción relacionada con alarma (hay 12 disponibles, de la 0 a la 11) y le ponemos un timepo para que se lance.
    + __Colisión__: Se ejecuta el código cuando hay una colisión (la que tu establezcas).
    + __Habitación exterior__: Ejecuta el código cuando el objeto que porta este evento sale fuera de la sala establecida.


### Clicker ###
Para hacer el juego del clicker hemos creado 4 objetos (el coco, un objeto que usaremos para coger las monedas del juego, la moneda y la gema) con sus respectivos sprites (exceptuando el objeto de cojermonedas), creamos tambien un fondo, donde se verán los objetos colocados.

__Coco (obj_coinblock)__

![spr_coinblock](https://gitlab.com/vlaguillo/Juegos_android/raw/master/Sprites/spr_coinblock.PNG)

Código: Cuando se pulse sobre este objeto creará una instancia (una moneda o una gema, dicha gema tendrá un 10% de probabilidades de crearse).

![obj_coinblock_pulsación](https://gitlab.com/vlaguillo/Juegos_android/raw/master/Objetos/obj_coinblock%20Pulsaci%C3%B3n.PNG)

Paso: Cuando la variable money_collected sea más grande o igual a 50 pasa al siguiente juego que es el brick breaker.

![obj_coinblock_paso](https://gitlab.com/vlaguillo/Juegos_android/raw/master/Objetos/obj_coinblock%20Paso.PNG)

__Monedas (obj_coin)__

![spr_coin](https://gitlab.com/vlaguillo/Juegos_android/raw/master/Sprites/spr_coin.PNG)

Código: Cuando se pasa el ratón por encima se suma 1 a la variable money_collected que se encuentra en el objeto cojermonedas. Una vez hecho esto la moneda se destruye.

![obj_coin_entrada_raton](https://gitlab.com/vlaguillo/Juegos_android/raw/master/Objetos/obj_coin%20Entrada%20de%20raton.PNG)

__Gemas (obj_gem)__

![spr_gem](https://gitlab.com/vlaguillo/Juegos_android/raw/master/Sprites/spr_gem.PNG)

Código: Cuando pasas por encima de la gema le suma su valor (10 monedas) a la variable money_collected, después, se destruye.

![obj_gem_entrada_raton](https://gitlab.com/vlaguillo/Juegos_android/raw/master/Objetos/obj_gem%20Entrada%20de%20raton.PNG)

__El objeto que recoje las monedas del juego (obj_coincollector)__

Tiene 2 eventos, uno de crear, que se ejecuta cuando se crea la instancia, y otro de dibujar, que crea la puntuación que consigues durante el juego. Es importante poner que este objeto sea persitente para que te siga al cambiar de habitación.

![obj_coincollector_crear](https://gitlab.com/vlaguillo/Juegos_android/raw/master/Objetos/obj_coincollector%20Crear.PNG)

![obj_coincollector_dibujar](https://gitlab.com/vlaguillo/Juegos_android/raw/master/Objetos/obj_coincollector%20Dibujar.PNG)

### Casino ###
Para hacer el casino necesitaremos crear un objeto para cada ruleta, para hacer esta tarea de manera sencilla es necesario crear un objeto que haga de padre (obj_slotParent) y asi evitamos tener que copiar el código para cada ruleta, ya que estos heredarán el código del objeto padre.

__Objeto Padre (obj_slotParent)__

![spr_slotParent](https://gitlab.com/vlaguillo/Juegos_android/raw/master/Sprites/spr_slot.PNG)

En este hay 5 eventos diferentes:

Crear: Se ejecuta al crear la instancia. Se establecen algunas variables como el run_speed o el totalImages que nos serán útiles más adelante.

![obj_slotParent_crear](https://gitlab.com/vlaguillo/Juegos_android/raw/master/Objetos/obj_slotparent%20Crear.PNG)

Paso: Comprueba a cada momento el código que hay dentro. Hace que las ruletas su muevan de abajo a arriba si las ruletas están en marcha.

![obj_slotParent_paso](https://gitlab.com/vlaguillo/Juegos_android/raw/master/Objetos/obj_slotparent%20Paso.PNG)

Alarma: Cuando las ruletas se paran se comprobará el resultado y si coinciden 3 iguales te darán monedas.

![obj_slotParent_alarma](https://gitlab.com/vlaguillo/Juegos_android/raw/master/Objetos/obj_slotparent%20Alarma0.PNG)

Dibujar: Hace que la animación de girar sea mucho más fluida.

![obj_slotParent_dibujar](https://gitlab.com/vlaguillo/Juegos_android/raw/master/Objetos/obj_slotparent%20Dibujar.PNG)

Pulsación global: Inicia la animación de las ruletas cada vez que el usuario pulsa la pantalla. Establece aleatoriamente la velocidad y el tiempo que la ruleta estará girando.

![obj_slotParent_pulsacion](https://gitlab.com/vlaguillo/Juegos_android/raw/master/Objetos/obj_slotparent%20Pulsacion%20global.PNG)

__objReward__

Hay 3 eventos diferentes:

Crear: Crea el mapa de recompensas.

![objReward_crear](https://gitlab.com/vlaguillo/Juegos_android/raw/master/Objetos/obj_reward%20Crear.PNG)

Dibujar: Cuando salgan 3 imágenes iguales te dirá "has ganado".

![objReward_dibujar](https://gitlab.com/vlaguillo/Juegos_android/raw/master/Objetos/obj_reward%20Dibujar.PNG)

Paso: Comprueba el resultado en el mapa de recompensas.

![objReward_paso](https://gitlab.com/vlaguillo/Juegos_android/raw/master/Objetos/obj_reward%20Paso.PNG)

__Ruleta 1 (objSlot1)__

Código: Hereda de obj_slotParent.

__Ruleta 2 (objSlot2)__

Código: Hereda de obj_slotParent.

__Ruleta 3 (objSlot3)__

Código: Hereda de obj_slotParent.

### Brick breaker ###

Nuestro brick breaker consta de un objeto que hará de bloque, otro que hará de pared, uno que hará de techo, otro que hará de paleta y para finalizar los dos tipos de bolas: en movimiento y en espera.

__Objeto Bloque (obj_brick1)__

![spr_brick1](https://gitlab.com/vlaguillo/Juegos_android/raw/master/Sprites/spr_brick1.PNG)

Tiene un solo evento que cada vez que colisiona con la bola aumenta en 20 el número de monedas conseguidas.

![obj_brick1_colision](https://gitlab.com/vlaguillo/Juegos_android/raw/master/Objetos/obj_brick1%20Colision-obj_ball.PNG)

__Objeto bola en movimiento (obj_ball)__

![spr_coinblock](https://gitlab.com/vlaguillo/Juegos_android/raw/master/Sprites/spr_coinblock.PNG)

Este objeto tiene 7 eventos:

Crear: Cuando la instancia es creada se le establece una direccion fija en dirección arriba a la derecha, con una fuerza de gravedad de 0.03 (para hacer que la bola vaya recta y no haga parabola).

![obj_ball_crear](https://gitlab.com/vlaguillo/Juegos_android/raw/master/Objetos/obj_ball%20Crear.PNG)

Paso: Se ejecuta durante cada instante que transcurre el juego, comprueba que haya ladrillos en la pista, si no los hay el juego finaliza y pasa al siguiente.

![obj_ball_paso](https://gitlab.com/vlaguillo/Juegos_android/raw/master/Objetos/obj_ball%20Paso.PNG)

Evento de colisión(contra obj_brick1): Se ejecuta cuando colisiona con el objeto especificado, invierte la dirección vertical dando una sensación de rebote y destruye el ladrillo.

![obj_ball_collision_brick1](https://gitlab.com/vlaguillo/Juegos_android/raw/master/Objetos/obj_ball%20Colision-obj_brick1.PNG)

Evento de colisión(contra obj_paddle): Establece una dirección variable según la zona de la paleta que toque, y le da una velocidad.

![obj_ball_collision_paddle](https://gitlab.com/vlaguillo/Juegos_android/raw/master/Objetos/obj_ball%20Colision-obj_paddle.PNG)

Evento de colisión(contra obj_roof): Cuando choca contra el techo de la sala hace rebotar la pelota.

![obj_ball_collision_roof](https://gitlab.com/vlaguillo/Juegos_android/raw/master/Objetos/obj_ball%20Colision-obj_roof.PNG)

Evento de colisión(contra obj_wall):Cuando choca contra la pared de la sala hace rebotar la pelota.

![obj_ball_collision_wall](https://gitlab.com/vlaguillo/Juegos_android/raw/master/Objetos/obj_ball%20Colision-obj_wall.PNG)

Habitacion exterior: Cuando el obj_ball sale de la habitación actual, se destruye y en su lugar se crea una instancia encima de la paleta de la bola en espera (obj_ballwait).

![obj_ball_habitación_exterior](https://gitlab.com/vlaguillo/Juegos_android/raw/master/Objetos/obj_ball%20Habitacion%20Exterior.PNG)

__Objeto Bola en espera (obj_ballwait)__

![spr_coinblock](https://gitlab.com/vlaguillo/Juegos_android/raw/master/Sprites/spr_coinblock.PNG)

Paso: Se le asigna una variable para usarla en el evento de colisión entre el objeto de la pelota y la paleta.

![obj_ball_wait_paso](https://gitlab.com/vlaguillo/Juegos_android/raw/master/Objetos/obj_ballwait%20Paso.PNG)

Pulsación: Cuando se pulsa la pantalla, la bola en espera es cambiada por la bola en movimiento para empezar a jugar.

![obj_ball_wait_pulsación](https://gitlab.com/vlaguillo/Juegos_android/raw/master/Objetos/obj_ballwait%20Pulsacion.PNG)

__Objeto paleta (obj_paddle)__

![spr_paddle](https://gitlab.com/vlaguillo/Juegos_android/raw/master/Sprites/spr_paddle.PNG)

Paso: Se ejecuta un código que permite mover la paleta usando en ratón.

![obj_paddle_paso](https://gitlab.com/vlaguillo/Juegos_android/raw/master/Objetos/obj_reward%20Paso.PNG)

Evento de colisión(contra obj_wall): Hace chocar contra la pared e impide que la paleta se salga de la sala.

![obj_paddle_colision](https://gitlab.com/vlaguillo/Juegos_android/raw/master/Objetos/obj_paddle%20Colision-obj_wall.PNG)

## 6. Implantación ##

Para poder llevar un juego hecho con Game Maker a la Play Store es necesario instalar:
+ [Java JDK](http://www.oracle.com/technetwork/java/javase/downloads/index.html)
+ [Android SDK](https://developer.android.com/studio/#downloads)
+ [Android NDK](https://developer.android.com/ndk/downloads/)

Primero instalamos el Java JDK que nos servirá para compilar nuestro juego en Android. 

Una vez instalado procederemos a instalar las SDK, que son las librerías de Android. Game Maker recomienda que el contenido del fichero esté todo en una misma carpeta cuyo nombre no contenga espacios ni caracteres especiales ya que de no ser así podría haber algun error. Para su configuración nos aparecerá una ventana con muchas opciones donde nos dirá lo que tenemos instalado, lo que no y lo que hace falta actualizar. Las Android SDK Tools y las Android SDK Platform-tools se pueden acualizar a la ultima versión. Tambien necesitarás actualizar las SDK Platform y Google APIs y en el apartado de extras hay que buscar la Android Support Repository y la Android Support Library

Ahora habrá que instalar el Android NDK que Game Maker lo utiliza para hacer compilaciones en línea de comandos.

Para finalizar habrá que configurar el Game Maker para Android. Para hacer esto nos iremos dentro del mismo programa a File -> Preferences -> Android y configuramos la __Keystore__. Una Keystore es un fichero con el cual te permite firmar tus aplicaciones con los datos que introduces en tu fichero, los datos son los siguientes:
+ Nombre del fichero
+ Contraseña
+ Alias
+ Contraseña del Alias
+ Nombre común
+ Compañia
+ Departamento
+ Ciudad
+ País

Una vez introducidos los datos hay que guardarlos toda la vida ya que para poder actualizar tu juego en un futuro requerirás de ellos. Luego dentro del mismo menu de Android hay que configurar las rutas de las carpetas que te has descargado del JDK, SDK y el NDK


## 7. Mantenimiento ##

Para poder exportar el proyecto en un apk es necesario comprar la licencia de Game Maker Studio 2 ya sea la permanente o la educativa.

Permanente:

![Precio permanente](https://gitlab.com/vlaguillo/Juegos_android/raw/master/Imagenes/Screenshot%20from%202018-05-25%2016-24-34.png)

Educativa:

![Precio educativa](https://gitlab.com/vlaguillo/Juegos_android/raw/master/Imagenes/Screenshot%20from%202018-05-25%2016-25-16.png)

## 8. Problemas ##

Al principio, el proyecto se hubiese hecho con kivy pero vimos de que era muy complicado y después de una semana y media pudimos cambiar la propuesta.

## 9. Conclusiones ##

Nos ha gustado mucho hacer este proyecto, hemos aprendido mucho y hemos sabido organizarnos aun habiendo perdido una semana.

## 10. Bibliografia ##

[Primera parte del tutorial clicker](https://youtu.be/u7zs78RzEGE)

[Segunda parte del tutorial clicker](https://youtu.be/mASzJHFvn9w)

[Tercera parte del tutorial clicker](https://youtu.be/HJl6tC5j_Hg)

[Cuarta parte del tutorial clicker](https://youtu.be/drWS47jdxQY)

[Tutorial básico de como hacer girar la ruleta ](https://youtu.be/hd2s7umrSoQ)

[Ejemplo del código de la ruleta](http://gamemakerblog.com/2017/09/18/gamemaker-tutorial-how-to-setup-a-simple-slot-machine-reward-system/)

[Primera parte del tutorial clicker](https://youtu.be/sfzShcq6x64)

[Segunda parte del tutorial clicker](https://youtu.be/AXF2_9-v4Kw)

[Android SDK](https://developer.android.com/studio/#downloads)

[Android NDK](https://developer.android.com/ndk/downloads/)

[Java JDK](http://www.oracle.com/technetwork/java/javase/downloads/index.html)

[Game Maker](https://www.yoyogames.com/gamemaker)

-----
[ir arriba](https://gitlab.com/vlaguillo/Juegos_android/blob/master/Documentaci%C3%B3n.md#juegos-android)