### Planificación del proyecto ###

|                       | Víctor Laguillo | Alba Callejas |
| :-------------------: | :-------------: | :-----------: |
| Consultar información | 27              | 27            |
| Casino                | 35              | 35            |
| Brick Breaker         | 30              | 30            |
| Clicker               | 13              | 15            |
| Documentación         | 8               | 6             |
| __Total__             | 113             | 113           |

---

|  Casino               |                 |
| :-------------------: | :-------------: |
| Diseño                | 10              |
| Programación          | 23              |
| Pruebas               | 2               |
| __Total__             | 35              |

---

|  Brick Breaker        |                 |
| :-------------------: | :-------------: |
| Diseño                | 7               |
| Programación          | 20              |
| Pruebas               | 3               |
| __Total__             | 30              |

---

|  Clicker              |                 |
| :-------------------: | :-------------: |
| Diseño                | 3               |
| Programación          | 10              |
| Pruebas               | 2               |
| __Total__             | 15              |

---

|  Documentacion        |                 |
| :-------------------: | :-------------: |
| Redactar              | 5               |
| Sacar imagenes        | 1               |
| Correcion             | 2               |
| __Total__             | 8               |